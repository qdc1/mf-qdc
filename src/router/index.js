console.log('router/index.js')
import { createRouter, createWebHashHistory, Route } from 'vue-router'

import DefaultLayout from '@/layouts/DefaultLayout'
import DefaultLayoutStandAlone from '@/standalone/layouts/DefaultLayoutStandAlone'
/*define const other in here*/

console.log(process.env.VUE_APP_STANDALONE_SINGLE_SPA)
const routes = [
  {
    path: '/',
    name: 'Home',
    component:
      process.env.VUE_APP_STANDALONE_SINGLE_SPA === 'true'
        ? DefaultLayoutStandAlone
        : DefaultLayout,
    redirect:  ( process.env.VUE_APP_STANDALONE_SINGLE_SPA!=='true' && (!localStorage.id_token || localStorage.id_token == '' )) ? 
      () => {
        window.location.href = process.env.dc+'/#/sc/login' 
        // return '/redirectingToLogin' // not important since redirecting
      }: '/app/dashboard',
    children: [
      {
        path: '/app/dashboard',
        name: 'Dashboard',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('@/views/Dashboard.vue'),
      },

      // QDC URL
      {
        path: '/qdc/quality-operational',
        name: 'Quality Operational',
        component: () => import('@/views/qdc/QualityOperational.vue'),
      },
      {
        path: '/qdc/gauge-control',
        name: 'Gauge Control',
        component: () => import('@/views/qdc/GaugeControlScreen.vue'),
      },
      {
        path: '/qdc/gauge-control/add',
        name: 'Gauge Control Add',
        component: () => import('@/views/qdc/GaugeControlForm.vue'),
      },
      {
        path: '/qdc/gauge-control/edit/:id',
        name: 'Gauge Control Edit',
        component: () => import('@/views/qdc/GaugeControlForm.vue'),
      },
      {
        path: '/qdc/gauge-control/detail/:id',
        name: 'Gauge Control Detail',
        component: () => import('@/views/qdc/GaugeControlDetail.vue'),
      },
      {
        path: '/qdc/calibration-item/:idGauge',
        name: 'Calibration Item',
        component: () => import('@/views/qdc/CalibrationItemScreen.vue'),
      },
      {
        path: '/qdc/calibration-item/add/:idGauge',
        name: 'Calibration Item Add',
        component: () => import('@/views/qdc/CalibrationItemForm.vue'),
      },
      {
        path: '/qdc/calibration-item/edit/:id',
        name: 'Calibration Item Edit',
        component: () => import('@/views/qdc/CalibrationItemForm.vue'),
      },
      {
        path: '/qdc/monthly-planning',
        name: 'Monthly Planning',
        component: () => import('@/views/qdc/MonthlyPlanning.vue'),
      },
      {
        path: '/qdc/daily-management',
        name: 'Daily Management',
        component: () => import('@/views/qdc/DailyManagementForm.vue'),
      },
      {
        path: '/qdc/monthly-planning-holiday',
        name: 'Monthly Planning Holiday',
        component: () => import('@/views/qdc/MonthlyPlanningHolidayScreen.vue'),
      },
      {
        path: '/qdc/monthly-planning-holiday/add',
        name: 'Monthly Planning Holiday Add',
        component: () => import('@/views/qdc/MonthlyPlanningHolidayForm.vue'),
      },
      {
        path: '/qdc/monthly-planning-holiday/edit/:id',
        name: 'Monthly Planning Holiday Edit',
        component: () => import('@/views/qdc/MonthlyPlanningHolidayForm.vue'),
      },
      {
        path: '/qdc/custom-planning',
        name: 'Custom Planning',
        component: () => import('@/views/qdc/CustomPlanningScreen.vue'),
      },
      {
        path: '/qdc/monthly-report',
        name: 'Monthly Report',
        component: () => import('@/views/qdc/MonthlyReportScreen.vue'),
      },

/*define other in here*/
    ],
  },
  {
    path: '/404',
    name: 'Page404',
    component: () => import('@/views/pages/Page404'),
  },
  {
    path: '/500',
    name: 'Page500',
    component: () => import('@/views/pages/Page500'),
  },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

export default router
